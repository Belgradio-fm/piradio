#!/bin/bash

# Pi radio CGI command.

echo -e "Content-Type: text/plain\n\n"

if [ -v REQUEST_METHOD -a "${REQUEST_METHOD:0:4}" -eq "HEAD" ]
then
    exit 0
else
    radiocmd "${QUERY_STRING}" 
fi

